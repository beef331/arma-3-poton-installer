#!/bin/python3
import os, subprocess
from sys import argv

home = os.path.expanduser("~")
steamDir = home+"/.local/share/Steam/steamapps/"
tfarModule = steamDir+"workshop/content/107410/620019431/teamspeak/task_force_radio.ts3_plugin"
proton = steamDir+"common/Proton 4.2/dist/bin/wine64"
try:
    script, teamspeakExeDir = argv
except:
    print("\nERROR: This script needs an argument\nPlease provide the path to the teamspeak installer. ex: ./install-teamspeak.py ~/Downloads/TeamSpeak3-Client-win64-3.2.5.exe\n")

if not os.path.isfile(teamspeakExeDir):
    print("ERROR: "+teamspeakExeDir+" File Not found")
    exit(1)

os.environ["WINEPREFIX"] = steamDir+"compatdata/107410/pfx"
os.environ["WINEESYNC"] = "1"

if not os.path.isdir(steamDir+"compatdata/107410/pfx/drive_c/users/steamuser/Local\ Settings/Application\ Data/Teamspeak\ 3\ Client"):
    print("Installing Teamspeak 3...")
    subprocess.call([proton, teamspeakExeDir])
    print("Teamspeak 3 finished installing")

packageInstaller = steamDir+"compatdata/107410/pfx/drive_c/users/steamuser/Local Settings/Application Data/TeamSpeak 3 Client/package_inst.exe"

teamspeakClient = steamDir+"compatdata/107410/pfx/drive_c/users/steamuser/Local Settings/Application Data/TeamSpeak 3 Client/ts3client_win64.exe"

input("\nThis scirpt will start Teamspeak to initialize it, please close Teamspeak after choosing a nickname\n Press enter to continue\n")
subprocess.call([proton, teamspeakClient])
input("\nPress Enter to install the TFAR plugin\n")
print("Installing TFAR plugin...")
subprocess.call([proton, packageInstaller, tfarModule])

print("Teamspeak 3 has finished installing")
exit(0)
