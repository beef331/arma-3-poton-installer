#!/bin/python3
import os, platform, subprocess, re, tarfile, vdf
def main():
    print ("Setting up...")
    

    def find_libraries():
        home = os.path.expanduser("~")
        if os.path.isfile(home+"/.steam/steam/steamapps/libraryfolders.vdf"):
            steam_library = str(home+"/.steam/steam/steamapps/libraryfolders.vdf")
            steamapps_dir = str(home+"/.steam/steam/steamapps")
        elif os.path.isfile(home+"/.local/share/Steam/steamapps/libraryfolders.vdf"):
            steam_library = str(home+"/.local/share/Steam/steamapps/libraryfolders.vdf")
            steamapps_dir = str(home+"/.local/share/Steam/steamapps")

        if not os.path.isdir(steamapps_dir+"common/Arma\\ 3"):
            with open(steam_library, 'r') as libvdf:
                libraries = re.findall(r'(?:[\w]+\/.+[\w]+)+', libvdf.read())
                for library in libraries:
                    if os.path.isdir("/" + library+"/steamapps/common/Arma 3"):
                        steamapps_dir = "/" +library+"/steamapps"
                        return steamapps_dir
        else:
            print("Steam library not found")
            exit(1)

        return steamapps_dir


    def install_arma(base_dir, steamapps_dir):

        home = os.path.expanduser("~")
        A3Proton = ""
        A3Native = ""
        A3Folder = str(steamapps_dir+"/common/Arma 3")
        
        with open(home+"/.steam/steam.pid") as f:
            while True:
                if os.path.exists("/proc/"+f.readline()):
                    input("Please exit Steam before continuing with the install\nPress enter to continue...")
                else:
                    break

        if os.path.isfile(A3Folder+"/arma3launcher.exe"):
            A3Proton = " --Installed"
        elif os.path.isfile(A3Folder+"/arma3.x86_64") or os.path.isfile(steamapps_dir+"/common/Arma 3-v1.82/arma3.x86_64"):
            A3Native = " --Installed"

        if A3Proton != "" or A3Native != "":
            
            options = ["1", "2"]
            print ("Please select Arma versions to install...\n")
            print ("1) install Arma 3 - Proton"+A3Proton)
            print ("2) install Arma 3 - v1.82 Native"+A3Native)
            if A3Proton == "" and A3Native == "":
                print ("3) install both versions")
                options.append("3")

            user_input = ""
            while (user_input not in options):
                user_input = input("Select an option: (1/2/3)\n")

        if user_input == "1":
            print ("\nInstalling Arma 3 - Proton...")

            if os.path.isfile(A3Folder+"/arma3.x86_64"):
                print ("Saving 1.82 version...")
                os.system("cp -r "+steamapps+"/common/Arma\\ 3/ "+steamapps+"/common/Arma 3-v1.82")

            steam_cmd(base_dir, steamapps_dir, "Arma 3", "107410", True)
            os.system("cp -r "+steamapps_dir+"/common/Arma\\ 3/steamapps "+steamapps+"/../")
            os.system("rm -r "+steamapps_dir+"/common/Arma\\ 3/steamapps")
            input("Installation completed, now right click Arma 3 and select properties, then force the game to use Proton 4.2\nPress enter to continue...")
        elif user_input == "2":
            print ("\nInstalling Arma 3 v1.82 Native...")
            with open(home+"/.steam/steam.pid") as f:
                while True:
                    if os.path.exists("/proc/"+f.readline()):
                        input("Please exit Steam before continuing with the install\nPress enter to continue...")
                    else:
                        break

            d = vdf.load(open(base_dir+"/config.vdf"))
            print (d)

            input("PAUSE")

            steam_cmd(base_dir, steamapps_dir, "Arma 3-v1.82", "107410", False)
            os.system("rm -r "+steamapps_dir+"/common/Arma\\ 3-v1.82/steamapps")
            input("Installation completed, you can add a shortcut for Arma 3-1.82 by using the \"add non-steam game\" feature and select arma3.x86_64 as the executable\nPress enter to continue...")
        elif user_input == "3":
            print ("\nInstalling Arma 3 Native/Proton...")
            steam_cmd(base_dir, steamapps_dir, "Arma 3", "107410", True)
            print ("\n\n\n")
            steam_cmd(base_dir, steamapps_dir, "Arma 3-v1.82", "107410", False)
            os.system("cp -r "+steamapps_dir+"/common/Arma\\ 3/steamapps "+steamapps+"/../")

            os.system("rm -r "+steamapps_dir+"/common/Arma\\ 3/steamapps")
            os.system("rm -r "+steamapps_dir+"/common/Arma\\ 3-v1.82/steamapps")

            print ("Both versions of Arma 3 have now been installed...")
            print ("Now right click Arma 3 and select properties, then force the game to use Proton 4.2")
            print ("\n(Optional) You can also add a shortcut for Arma 3-1.82 by using the \"add non-steam game\" feature and select arma3.x86_64 as the executable")
            input("Press enter to continue...")


    def detect(distro):
        linuxVer = platform.release()
        linuxVer = linuxVer.lower()

        if platform.system() == "Linux":
            if distro.lower() in linuxVer:
                usrInput = input(distro+" was detected, is this correct?: (Y/N)\n")
                if usrInput.lower() == "y":
                    print ("Installing and setting up Arma 3 Proton...")
                    return True
                else:
                    print ("Exiting installer...")
                    exit(1)
            else:
                return False
        else:
            print (platform.system()+" is not supported by this installer, exiting...")
            exit(1)

    def install(distro, base_dir, steamapps_dir):

        def rename_exes(base_dir, steamapps_dir):
            print ("Renaming Arma 3 exe's...")
            arma3_dir = str(steamapps_dir+"/common/Arma 3")
            if os.path.isfile(arma3_dir+"/arma3launcher.exe") and os.path.isfile(arma3_dir+"/arma3_x64.exe"):
                os.system("mv "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe.backup")
                os.system("mv "+steamapps_dir+"/common/Arma\ 3"+"/arma3_x64.exe "+steamapps_dir+"/common/Arma\ 3"+"/arma3launcher.exe")
            else:
                print ("Warning: arma3launcher.exe or arma3_x64.exe not found, did you already rename it?")

        print ("Installing...")
        if distro == "Debian" or distro == "Ubuntu":
            print ("Installing dependencies...")
            if os.system("sudo apt install mesa-vulkan-drivers vulkan-utils") != 0:
                print ("An error occurred, Aborting install...")
                exit(1)
            install_proton(base_dir, steamapps_dir)
            setup_prefix(base_dir, steamapps_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! enjoy your game.")
            exit(0)

        elif distro == "Arch":
            install_proton(base_dir, steamapps_dir)
            setup_prefix(base_dir, steamapps_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete!")
            exit(0)

        elif distro == "Fedora":
            install_proton(base_dir, steamapps_dir)
            setup_prefix(base_dir, steamapps_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! *tips fedora*.")
            exit(0)

        else:
            install_proton(base, steamapps_dir)
            setup_prefix(base_dir, steamapps_dir)
            rename_exes(base_dir, steamapps_dir)

            print ("Install complete! *insert distro joke here*")

    def setup_prefix(base_dir, steamapps_dir):
        if not os.path.isdir(base_dir+"/Resources/winetricks"):
            print ("\nDownloading winetricks...\n")
            os.mkdir(base_dir+"/Resources/winetricks")
            os.system('wget -P '+base_dir+'/Resources/winetricks/ "https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks"')
            os.chmod(base_dir+'/Resources/winetricks/winetricks', 0o775)
            
        print ("\nSetting up prefix...\n") 
        proton_dir = str(steamapps_dir+"/common/Proton\\ 4.2")
        
        while not os.path.isfile(steamapps_dir+"/compatdata/107410/pfx/user.reg"):
                input("\nThe prefix has not yet been initialized, please click 'play' in steam and start Arma 3 to perform first time setup, you might get errors but you can ignore them.\n\nPress enter to continue...")


        os.system("cp "+ base_dir+"/Resources/imagehlp.dll.so "+proton_dir+"/dist/lib64/wine/imagehlp.dll.so")
        
        os.environ["WINEDLLPATH"] = str(proton_dir+"/dist/lib64/wine:"+proton_dir+"/dist/lib/wine")
        os.environ["PATH"] = str(proton_dir+"/dist/bin/:"+proton_dir+"/dist/lib/:"+proton_dir+"/dist/lib64/:/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin:/snap/bin")
        os.environ["WINEPREFIX"] = str(steamapps_dir+"/compatdata/107410/pfx/")

        winetricks = str(base_dir+"/Resources/winetricks/winetricks")

        # set override dlls
        # refuses to work if the extra indent isn't there, no idea why
        def override_dll(dllName, loadType, insertPoint):
                with open(steamapps_dir+"/compatdata/107410/pfx/user.reg", 'r+') as f:
                    text = f.read()
                    while True:
                        with open(steamapps_dir+"/compatdata/107410/pfx/user.reg", 'r+') as nf:
                            if dllName in nf.read():
                                break
                            insertPoint = "\""+ insertPoint+"\"=\"native,builtin\""
                            dllOverride = "\""+ dllName+"\"=\""+loadType+"\""
                            text = re.sub(insertPoint, insertPoint+'\n'+dllOverride, text)
                            nf.seek(0)
                            nf.truncate()
                            nf.write(text)
                            print ("Trying to overwrite "+dllName+"...")
                            os.system("sleep 2")
                        with open(steamapps_dir+"/compatdata/107410/pfx/user.reg", 'r+') as cf:
                            if dllName in cf.read():
                                break
                            else:
                                continue

        override_dll("imagehlp", "native,builtin", "concrt140")
        override_dll("api-ms-win-crt-private-l1-1-0", "native,builtin", "api-ms-win-crt-math-l1-1-0")

        # need to call winetricks twice to fix some bug with user.reg, possible fix
        # is to run override dll before the winetricks process:::: FIXED
        subprocess.call([winetricks, "d3dcompiler_43", "d3dx10_43", "d3dx11_43", "xact_x64"])


    def steam_cmd(base_dir, steamapps_dir, app_name, app_id, force_windows):
        if not os.path.isdir(base_dir+"/Resources/steamcmd"):
            print ("\nDownloading steamcmd...\n")
            os.mkdir(base_dir+"/Resources/steamcmd")
            os.system('wget "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" && tar -xf steamcmd_linux.tar.gz -C '+base_dir+'/Resources/steamcmd')

        steam_user = input("Please enter your Steam username: ")
        steamcmd = base_dir+"/Resources/steamcmd/steamcmd.sh"
        login = "+login "+steam_user
        install_dir = str('+force_install_dir "'+steamapps_dir+"/common"+'/'+app_name+'"')
        app_update = "+app_update "+app_id
        if force_windows == True:
            platform_type = "+@sSteamCmdForcePlatformType windows"
        else:
            platform_type = "+@sSteamCmdForcePlatformType linux"
        subprocess.call([steamcmd, platform_type, login, install_dir, app_update, "validate", "+quit"])
        return

    def install_proton(base_dir, steamapps_dir):
        if not os.path.isfile( steamapps_dir+"/common/Proton 4.2/proton"):
            user_input = ""
            while  user_input not in ["y","n"]:
                user_input = input("Proton 4.2 does not appear to be installed, install now? (Y/N)\n").lower()
            if user_input != "y":
                "Exiting install..."
                exit(1)
            else:
                print ("\nInstalling Proton...\n")
                proton_versions = [["1054830", "Proton 4.2"], ["996510", "Proton 3.16 Beta"], ["930400", "Proton 3.7 Beta"]]
                while True:
                    print("Which version of Proton would you like to install? (1/2/3)")
                    print ("0) Proton 4.2 (default)")
                    print ("1) Proton 3.16 Beta")
                    print ("2) Proton 3.7 Beta")
                    print ("THIS IS A FEATURE STILL BEING ADDED")
                    user_input = input()
                    if user_input in ["0","1","2"]:
                        break
                    elif user_input == "":
                        user_input = "0"
                user_input = "0" #FIXME Make proton_versions, proton_dir, proton_dir_cli_safe global
                                     #      and change all functions to use these
                steam_cmd(base_dir, steamapps_dir, proton_versions[int(user_input)][1], proton_versions[int(user_input)][0], False)
                print ("\nExtracting dist...\n")
                proton_dir = steamapps_dir+"/common/"+proton_versions[int(user_input)][1]
                proton_dir_cli_safe = proton_dir.replace(" ", "\\ ")
                tar = tarfile.open(proton_dir+"/proton_dist.tar.gz", mode="r:gz")
                tar.extractall(path=proton_dir+"/dist")
                tar.close()
                os.system("cp "+proton_dir_cli_safe+"/version "+proton_dir_cli_safe+"/dist/")
                os.system("cp -r "+proton_dir_cli_safe+"/steamapps "+steamapps_dir+"/../")

    # MAIN
     
    steamapps_dir = find_libraries()
    base_dir = os.getcwd()
    print ("Finding Steam library locations...")
    find_libraries()
    print ("Detecting OS and distro...")
    for distro in ["Debian", "Ubuntu", "PopOS", "Fedora", "Arch", "Void"]:
        if detect(distro):
            #install_arma(base_dir, steamapps_dir) # FIXME need to edit config.vdf/ToolMapping() to
            #                                      # install the correct version of arma 3
            install(distro, base_dir, steamapps_dir)
            break
    else:
        user_input = input(platform.release()+" Is not yet supported, continue anyway? (Y/N)\n")
        if user_input.lower() != "n":
            install("Arch", base_dir, steamapps_dir)
        else:
            exit(0)

if __name__ == '__main__':
    main()

# vim: set syntax=python:
